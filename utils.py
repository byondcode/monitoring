from flask import jsonify
from time import time


class RenderableException(Exception):
    def __init__(self, *args, **kwargs):
        self.renderable_message = self.__class__.renderable_message


class InvalidMethodParams(RenderableException):
    renderable_message = 'Invalid arguments'


class UnsupportedServerVersion(RenderableException):
    renderable_message = 'Can\'t show widget for this type of server'


def _error_response(error_type, error_message=None):
    return {'error': {'type': error_type, 'message': error_message or ''}}


def error_response(error):
    if isinstance(error, Exception):
        return _error_response(error.__class__.__name__, str(error))
    if isinstance(error, tuple):
        return _error_response(*error)
    raise NotImplementedError


def render_exceptions(func):
    def new_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except RenderableException as ex:
            return jsonify(error_response(ex))
    return new_func


def cached(lifetime_seconds):
    def decorator(func):
        cache = {}

        def new_func(*args, **kwargs):
            key = (args, tuple(kwargs.items()))
            try:
                created_at, value = cache[key]
                if (time() - created_at) > lifetime_seconds:
                    raise KeyError
                else:
                    return value
            except KeyError:
                value = func(*args, **kwargs)
                cache[key] = (time(), value)
                return value
            return func(*args, **kwargs)
        return new_func
    return decorator
