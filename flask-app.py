from flask import Flask, request, jsonify

from byondserver import get_status
from utils import render_exceptions, InvalidMethodParams, RenderableException,\
    UnsupportedServerVersion
import templates


app = Flask(__name__)


@app.route('/')
def main_page():
    return '''This is a service to check any byond server status.
You can get a status of a particular website by performing such a request:
/status?server=play.ss13.pw:1937
Which will return you a JSON object.
Try it out!
'''


@app.route('/status')
@render_exceptions
def get_server_status():
    try:
        server = request.args['server']
    except KeyError:
        raise InvalidMethodParams(
            'You need to specify server address in `server` param')
    status = get_status(server)
    return jsonify({'status': status})


@app.route('/widget')
def widget():
    render_context = {'server_name': request.args.get('name')}
    try:
        server_address = request.args['server']
        status = get_status(server_address)
        render_context.update({
            'address': server_address,
            'version': status['version'],
        })
        if status['version'] == 'InterBay':
            minutes, seconds = map(int, status['roundduration'].split(':'))
            render_context.update({
                'online': status['players'],
                'playtime': minutes * 60 + seconds
            })
        elif status['version'] == '/tg/ Station 13':
            render_context.update({
                'online': status['players'],
                'playtime': status['round_duration']
            })
        else:
            raise UnsupportedServerVersion
    except RenderableException as ex:
        render_context.update({'exception': ex})
    except KeyError:
        pass
    return templates.widget(render_context)


if __name__ == '__main__':
    app.run('127.0.0.1', 3000)
