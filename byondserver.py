from socket import socket, AF_INET, SOCK_STREAM, \
    timeout, error, herror, gaierror
from urllib.parse import unquote

from utils import RenderableException, cached


class InvalidServerAddress(RenderableException):
    renderable_message = 'Invalid or inaccessible server address'


class ServerOffline(RenderableException):
    renderable_message = 'Server is offline'


def _parse_form_urlencoded(data):
    def _unquote(val):
        return unquote(val).replace('+', ' ')

    def get_pair(item):
        item = tuple(map(_unquote, item.split('=')))
        try:
            key, value = item
            return key, value
        except ValueError:
            return item[0], None

    items = map(get_pair, data.decode().split('&'))
    dict_ = {}
    for key, val in items:
        if key in dict_:
            if isinstance(dict_[key], list):
                dict_[key].append(val)
            else:
                dict_[key] = [dict_[key], val]
        else:
            dict_[key] = val
    return dict_


def _get_status(server_address):
    try:
        host, port = server_address.split(':')
        port = int(port)
    except ValueError:
        raise InvalidServerAddress(server_address)
    sock = socket(AF_INET, SOCK_STREAM)
    sock.settimeout(2.0)
    sock.connect((host, port))
    sock.send(
        b'\x00\x83\x00\x0D\x00\x00\x00\x00\x00\x3f\x73\x74\x61\x74\x75\x73\x00'
    )
    response = sock.recv(10000)
    return _parse_form_urlencoded(response[5:-1])


@cached(5)
def get_status(server_address):
    try:
        return _get_status(server_address)
    except (timeout, error) as ex:
        raise ServerOffline(str(ex))
    except (herror, gaierror) as ex:
        raise InvalidServerAddress(str(ex))
