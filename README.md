This is a service to check any byond server status.<br>
You can get a status of a particular website by performing such a request:<br>
/status?server=play.ss13.pw:1937<br>
Which will return you a JSON object.<br>
Try it out!<br>

Installing:<br>
<pre>pip3 install Flask
python3 flask-app.py</pre>
Example for using on site:
<pre>iframe class="gamebanner" src="/widget?server=play.ss13.pw:1937&amp;name=Solar Station" style="border:none;width:250px;height:90px"> /iframe</pre>