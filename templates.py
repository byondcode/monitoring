from jinja2 import FileSystemLoader, Environment


def _helper_format_seconds(seconds):
    minutes, seconds = divmod(int(seconds), 60)
    hours, minutes = divmod(minutes, 60)
    if hours:
        return "{}h {}m {}s".format(hours, minutes, seconds)
    if minutes:
        return "{}m {}s".format(minutes, seconds)
    return "{}s".format(seconds)


loader = FileSystemLoader('./templates')
env = Environment(autoescape=True, loader=loader)
env.filters['readable_time'] = _helper_format_seconds


def widget(context):
    return env.get_template('widget.html').render(**context)
